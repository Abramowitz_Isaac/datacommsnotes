History
-------
* 1947 - Transistors -> miniaturization
* 1959 - "integrated circuit" -> chips
* 1971 - Intel -> 1<sup>st</sup> microprocessor -> CPU on a chip
	* AMD reverse engineers Intel chips (making clones)

Telephone
---------
* Earliest -> point-to-point
	* Too many redundant connections, leads to: ->
* The switchboard

Telephony
---------
* Tel system engineered for max 12% utilization

<details>
	<summary>telcos have large UPS</summary>

	(uninterpretable power supplies)
</details>

Metrics
-------
<details>
    <summary><b>Response Time:</b> -> measures <b>interactive systems</b> -> have capacity for <b>dialogue</b> between user & system</summary>
    
    = time the answer returned - time the query is sent
</details>

<details>
    <summary><b>Turnaround Time</b> -> measures batch systems</summary>
    
	= time the job returns - time the job is sent
</details>

<details>
    <summary><b>Throughput</b></summary>
    
    = work/time
	Ex: 2000 jobs / second 5M tps
	(Transactions per second)
</details>

<details>
    <summary><b>Availability</b></summary>
    
    = timeAvailable / statedTimeAvailable * 100
</details>

<details>
    <summary><b>Utilization</b></summary>
    
    = timeUsed / timeAvailable * 100
</details>